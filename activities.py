import csv
import random
import string

def create() -> None:

    list_activity_type = []
    list_activity_level = []
    list_activity = []
    list_bridge_activities = []

    temp_list_activity_type = [
        'Career',
        'Award',
        'Sport',
        'Innovation',
        'Cultural',
        'Academic',
        'Volunteer',
        'Entrepreneurship',
        'Leadership'
    ]

    for i, v in enumerate(temp_list_activity_type):
        list_activity_type.append([str(i+1), v])
    
    temp_list_activity_level = [
        'Faculty',
        'University',
        'National',
        'International'
    ]

    for i, v in enumerate(temp_list_activity_level):
        list_activity_level.append([str(i+1), v])

    # activity seed data
    temp_list_activity = []
    count = 0
    while count < 20:
        activity_name = 'Activity UTM' + ''.join(random.choices(string.ascii_uppercase + string.digits, k=4))
        temp_list_activity.append(activity_name)
        chosen_activity_type = random.randint(1, len(list_activity_type))
        chosen_activity_level = random.randint(1, len(list_activity_level))
        chosen_achievement = random.choice(['gold', 'silver', 'merit', 'distinction', 'participation'])
        list_activity.append([count+1, activity_name, chosen_achievement, chosen_activity_level, chosen_activity_type])
        count += 1

    with open('out/activity_type.csv', 'w', newline='\n') as f:
        header = ['id', 'name']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_activity_type)

    with open('out/activity_level.csv', 'w', newline='\n') as f:
        header = ['id', 'level']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_activity_level)

    with open('out/activity.csv', 'w', newline='\n') as f:
        header = ['id', 'name', 'achievement', 'activity_level_id', 'activity_type_id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_activity)
