# Generate Data and Load into MySQL
Source code structures
- create_all.py will create the csv in out/ directory
- load_dw.py will load the csv into MySQL
- configure in load_dw the db details

Install and run
- to install all the necessary modules in python, in terminal run:  
    `pip install -r requirements.txt`  
- to install pymysql, 
    `pip uninstall PyMySQL`
    `pip install PyMySQL` 
- then, run  
    `python main.py`  
    to run the program.