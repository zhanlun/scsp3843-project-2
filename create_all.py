import discipline
import student
import activities
import emp_status
import cgpa
import os

def run() -> None:
    # create directory for output
    if not os.path.exists('out'):
        os.mkdir('out')

    total = 2048

    discipline.create()
    activities.create()
    emp_status.create()
    # num of cgpa == num of student
    cgpa.create(total)
    student.create()

if __name__ == '__main__':
    run()