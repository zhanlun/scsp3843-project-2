import csv
import random

def create() -> None:
    list_student = []
    list_program_id = []
    list_emp_status_id = []
    list_activity_id = []
    list_cgpa_id = []
    list_bridge_activity = []
    list_course_id = []
    list_bridge_courses_taken = []

    with open('out/program.csv') as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                list_program_id.append(row[0])
            line_count += 1

    with open('out/emp_status.csv') as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                list_emp_status_id.append(row[0])
            line_count += 1

    with open('out/activity.csv') as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                list_activity_id.append(row[0])
            line_count += 1

    with open('out/course.csv') as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                list_course_id.append(row[0])
            line_count += 1

    with open('out/cgpa.csv') as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                pass
            else:
                list_cgpa_id.append(row[0])
            line_count += 1

    # student seed data
    count = 0
    id_bridge_act = 1
    id_bridge_course = 1

    while count < len(list_cgpa_id):
        current_id = count + 1

        program_id = random.choice(list_program_id)
        emp_status_id = random.choice(list_emp_status_id)

        for _ in range(random.randint(3, 10)):
            activity_id = random.choice(list_activity_id)
            list_bridge_activity.append([id_bridge_act, current_id, activity_id])
            id_bridge_act += 1
        
        for _ in range(random.randint(10, 44)):
            course_id = random.choice(list_course_id)
            list_bridge_courses_taken.append([id_bridge_course, current_id, course_id])
            id_bridge_course += 1
        
        list_student.append([current_id, current_id, emp_status_id, current_id, program_id, current_id])
        count += 1

    with open('out/student.csv', 'w', newline='\n') as f:
        header = ['id', 'cgpa_id', 'employment_status_id', 'activity_joined_id', 'program_id', 'courses_taken_id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_student)

    with open('out/activities_joined.csv', 'w', newline='\n') as f:
        header = ['id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows([str(k+1)] for k in range(len(list_cgpa_id)))

    with open('out/bridge_activities.csv', 'w', newline='\n') as f:
        header = ['id', 'activities_joined_id', 'activity_id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_bridge_activity)

    with open('out/courses_taken.csv', 'w', newline='\n') as f:
        header = ['id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows([str(k+1)] for k in range(len(list_cgpa_id)))

    with open('out/bridge_courses_taken.csv', 'w', newline='\n') as f:
        header = ['id', 'courses_taken_id', 'course_id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_bridge_courses_taken)