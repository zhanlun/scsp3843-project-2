import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
import csv
import itertools


def load_from_csv_to_db(table_name: str, cursor) -> None:
    results = cursor.execute('SELECT * from {} limit 1'.format(table_name))
    if not results:
        with open('out/{}.csv'.format(table_name)) as f:
            csv_reader = csv.reader(f, delimiter=',')
            line_count = 0
            header = []
            values_template = ''
            for row in csv_reader:
                if line_count == 0:
                    header = ','.join(row)
                    values_template = ','.join(itertools.repeat('%s', len(row)))
                    pass
                else:
                    sql = '''INSERT INTO {}({})
                        VALUES ({})
                    '''.format(table_name, header, values_template)
                    cursor.execute(sql, row)
                line_count += 1


def run() -> None:
    db_name = 'uni_grad'

    try:
        db = MySQLdb.connect("localhost", "root", "123456", db_name)
    except:
        db = MySQLdb.connect("localhost", "root", "123456")
        cursor = db.cursor()
        cursor.execute('CREATE DATABASE IF NOT EXISTS {}'.format(db_name))
        db.close()
        db = MySQLdb.connect("localhost", "root", "123456", db_name)

    cursor = db.cursor()
    cursor.execute('SET FOREIGN_KEY_CHECKS = 0')

    table_name = 'activity_type'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        name VARCHAR(255)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'activity_level'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        level VARCHAR(255)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'activity'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        name VARCHAR(255),
        achievement VARCHAR(255),
        activity_level_id INT,
        activity_type_id INT,
        FOREIGN KEY (activity_level_id) REFERENCES activity_level(id),
        FOREIGN KEY (activity_type_id) REFERENCES activity_type(id)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'activities_joined'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'bridge_activities'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        activities_joined_id INT,
        activity_id INT,
        FOREIGN KEY (activities_joined_id) REFERENCES activities_joined(id),
        FOREIGN KEY (activity_id) REFERENCES activity(id)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'cgpa'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        cgpa DOUBLE(4, 2)
    )
    '''.format(table_name)
    cursor.execute(sql)
    results = cursor.execute('SELECT * from {} limit 1'.format(table_name))
    load_from_csv_to_db(table_name, cursor)


    table_name = 'discipline'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        discipline VARCHAR(255)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)


    table_name = 'emp_status'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        status VARCHAR(255)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)


    table_name = 'program'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        name VARCHAR(255),
        discipline_id INT,
        FOREIGN KEY (discipline_id) REFERENCES discipline(id)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'course'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        name VARCHAR(255),
        discipline_id INT,
        FOREIGN KEY (discipline_id) REFERENCES discipline(id)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'courses_taken'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)


    table_name = 'bridge_courses_taken'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        courses_taken_id INT,
        course_id INT,
        FOREIGN KEY (courses_taken_id) REFERENCES courses_taken(id),
        FOREIGN KEY (course_id) REFERENCES course(id)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)



    table_name = 'student'
    sql = 'DROP TABLE IF EXISTS {}'.format(table_name)
    cursor.execute(sql)
    sql = '''CREATE TABLE IF NOT EXISTS {} (
        id INT KEY,
        cgpa_id INT,
        employment_status_id INT,
        activity_joined_id INT,
        program_id INT,
        courses_taken_id INT,
        FOREIGN KEY (cgpa_id) REFERENCES cgpa(id),
        FOREIGN KEY (employment_status_id) REFERENCES emp_status(id),
        FOREIGN KEY (activity_joined_id) REFERENCES activity_joined(id),
        FOREIGN KEY (program_id) REFERENCES program(id),
        FOREIGN KEY (courses_taken_id) REFERENCES courses_taken(id)
    )
    '''.format(table_name)
    cursor.execute(sql)
    load_from_csv_to_db(table_name, cursor)


    cursor.execute('SET FOREIGN_KEY_CHECKS = 1')
    db.commit()
    cursor.close()
    db.close()


if __name__ == '__main__':
    run()