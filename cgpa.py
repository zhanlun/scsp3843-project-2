import csv
import random

def create(total: int = 100) -> None:
    list_cgpa = []

    for i in range(total):
        cgpa = 2.0 + random.random() * 2.0
        cgpa = round(cgpa, 2)
        list_cgpa.append([str(i+1), str(cgpa)])

    with open('out/cgpa.csv', 'w', newline='\n') as f:
        header = ['id', 'cgpa']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_cgpa)