import csv
import random
import string

num_courses = 255

def create() -> None:
    values = {
        'Science & Technology': {
            'program': [
                'Math',
                'Physics',
                'Chemistry',
                'Biology',
                'Electrical Engineering',
                'Mechanical Engineering',
                'Chemical Engineering',
                'Civil Engineering',
                'Computer Science'
            ]
        },
        'Arts & Humanities & Social Science': {
            'program': [
                'History',
                'Arts',
                'Graphic Design',
                'Communication',
                'Education',
                'Geography',
                'Political Science'
            ]
        },
        'Business': {
            'program': [
                'Accounting',
                'Business Administration',
                'Economics',
                'Finance'
            ]
        },
        'Medical': {
            'program': [
                'Medicine',
                'Dentistry',
                'Veterinary',
                'Nursing',
                'Physiotherapy'
            ]
        }
    }


    list_discipline = []
    list_program = []
    list_course = []

    id_discipline = 1
    id_program = 1
    id_course = 1

    for key, val in values.items():
        list_discipline.append([str(id_discipline), key])

        # loop program
        for idx, program in enumerate(val['program']):
            list_program.append([str(id_program), program, str(id_discipline)])
            id_program += 1

        id_discipline += 1

    for i in range(num_courses):
        chosen_discipline_id = random.randint(1, len(values))
        name = 'S' + ''.join(random.choices(string.ascii_uppercase, k=3)) + ''.join(random.choices(string.digits, k=4))
        list_course.append([str(i), name, chosen_discipline_id])

    with open('out/discipline.csv', 'w', newline='\n') as f:
        header = ['id', 'discipline']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_discipline)

    with open('out/program.csv', 'w', newline='\n') as f:
        header = ['id', 'name', 'discipline_id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_program)

    with open('out/course.csv', 'w', newline='\n') as f:
        header = ['id', 'name', 'discipline_id']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_course)