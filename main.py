import create_all
import load_dw

if __name__ == '__main__':
    create_all.run()
    load_dw.run()