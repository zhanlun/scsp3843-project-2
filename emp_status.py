import csv

def create() -> None:
    list_status = [
        ['1', 'Unemployed'],
        ['2', 'Employed']
    ]

    with open('out/emp_status.csv', 'w', newline='\n') as f:
        header = ['id', 'status']
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        wr.writerow(header)
        wr.writerows(list_status)